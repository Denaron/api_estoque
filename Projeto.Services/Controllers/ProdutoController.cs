﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Projeto.Services.Models; //importando
using Projeto.Infra.Data.Contracts;
using AutoMapper;
using Projeto.Entidades;

namespace Projeto.Services.Controllers
{
    [RoutePrefix("api/produto")]
    public class ProdutoController : ApiController
    {

        private readonly IUnitOfWork unitOfWork;

        public ProdutoController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("cadastrar")]
        public HttpResponseMessage Post(ProdutoCadastroModel model)
        {
            try
            {

                if(ModelState.IsValid)
                {

                    Produto p = Mapper.Map<Produto>(model);

                    unitOfWork.ProdutoRepository.Insert(p);

                    return Request.CreateResponse(HttpStatusCode.OK, "Produto cadastrado com sucesso.");

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Forbidden, ModelState);
                }
    
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpPut]
        [Route("atualizar")]
        public HttpResponseMessage Put(ProdutoEdicaoModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Produto p = Mapper.Map<Produto>(model);

                    unitOfWork.ProdutoRepository.Update(p);

                    return Request.CreateResponse(HttpStatusCode.OK, "Produto atualizado com sucesso.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Forbidden, ModelState);
                }

            }
            
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpDelete]
        [Route("excluir")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                Produto p = unitOfWork.ProdutoRepository.FindById(id);

                unitOfWork.ProdutoRepository.Delete(p);

                return Request.CreateResponse(HttpStatusCode.OK, "Produto excluído com sucesso.");
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("obtertodos")]
        public HttpResponseMessage GetAll()
        {
            try
            {
                List<ProdutoConsultaModel> lista = new List<ProdutoConsultaModel>();

                foreach (Produto p in unitOfWork.ProdutoRepository.FindAll())
                {
                    lista.Add(Mapper.Map<ProdutoConsultaModel>(p));
                }

                return Request.CreateResponse(HttpStatusCode.OK, lista);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("obterporid")]
        public HttpResponseMessage GetById(int id)
        {
            try
            {

                Produto p = unitOfWork.ProdutoRepository.FindById(id);

                ProdutoConsultaModel model = Mapper.Map<ProdutoConsultaModel>(p);

                return Request.CreateResponse(HttpStatusCode.OK, model);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
