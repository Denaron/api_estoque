﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projeto.Entidades;
using Projeto.Infra.Data.Contracts;
using Projeto.Infra.Data.Context;

namespace Projeto.Infra.Data.Repositories
{
    public class EstoqueRepository : BaseRepository<Estoque>, IEstoqueRepository
    {

        //declarando um atributo para a classe DataContext.. 
        private readonly DataContext context;

        public EstoqueRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
