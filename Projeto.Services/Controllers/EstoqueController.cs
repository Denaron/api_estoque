﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Projeto.Services.Models; //classes de modelo..
using Projeto.Infra.Data.Contracts;
using Projeto.Entidades;
using AutoMapper;

namespace Projeto.Services.Controllers
{
    [RoutePrefix("api/estoque")]
    public class EstoqueController : ApiController
    {
        //atributo.. 
        private readonly IUnitOfWork unitOfWork;

        //construtor para injeção de dependencia..
        public EstoqueController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("cadastrar")] //URL: /api/estoque/cadastrar 
        public HttpResponseMessage Post(EstoqueCadastroModel model)
        {
            try
            {
                //verificação validações da model.. 
                if (ModelState.IsValid) //testar as validações da model.. 
                {
                    Estoque e = Mapper.Map<Estoque>(model);

                    unitOfWork.EstoqueRepository.Insert(e);

                    //requisição bem-sucedida (HTTP 200 - Ok) 
                    return Request.CreateResponse(HttpStatusCode.OK, "Estoque cadastrado com sucesso.");
                }
                else
                {
                    //Erro HTTP 403 (Requisição proibida)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, ModelState);
                }
            }
            catch (Exception e)
            {
                //requisição inválida (HTTP 400 - BadRequest) 
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpPut]
        [Route("atualizar")] //URL: /api/estoque/atualizar 
        public HttpResponseMessage Put(EstoqueEdicaoModel model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Estoque e = Mapper.Map<Estoque>(model);

                    unitOfWork.EstoqueRepository.Update(e);

                    return Request.CreateResponse(HttpStatusCode.OK, "Estoque atualizado com sucesso.");

                }
                else
                {

                    return Request.CreateResponse(HttpStatusCode.Forbidden, ModelState);

                } 
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpDelete]
        [Route("excluir")] //URL: /api/estoque/excluir?id={0} 
        public HttpResponseMessage Delete(int id)
        {
            try
            {
              
                    Estoque e = unitOfWork.EstoqueRepository.FindById(id);
                    unitOfWork.EstoqueRepository.Delete(e);

                    return Request.CreateResponse(HttpStatusCode.OK, "Estoque excluído com sucesso.");
                     
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("obtertodos")]  //URL: /api/estoque/obtertodos 
        public HttpResponseMessage GetAll()
        {
            try
            {
                List<EstoqueConsultaModel> lista = new List<EstoqueConsultaModel>();

                foreach(Estoque e in unitOfWork.EstoqueRepository.FindAll())
                {
                    lista.Add(Mapper.Map<EstoqueConsultaModel>(e));
                }

                return Request.CreateResponse(HttpStatusCode.OK, lista);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("obterporid")] //URL: /api/estoque/obterporid?id={0} 
        public HttpResponseMessage GetById(int id)
        {
            try
            {
                Estoque e = unitOfWork.EstoqueRepository.FindById(id);

                EstoqueConsultaModel model = Mapper.Map<EstoqueConsultaModel>(e);

                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
