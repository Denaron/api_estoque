﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Projeto.Entidades;

namespace Projeto.Infra.Data.Mappings
{
    public class EstoqueMap : EntityTypeConfiguration<Estoque>
    {
        public EstoqueMap()
        {
            HasKey( e => e.IdEstoque);

            Property(e => e.Nome)
                .HasMaxLength(50)
                .IsRequired();

        }
    }
}
