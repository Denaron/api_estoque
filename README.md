# api_estoque

Desenvolvimento REST com WebApi, EntityFramework,  AngularJS, SimpleInjector e SOLID. 

Tecnologias: 
- Asp.Net WebApi 
- Entity Framework 
- Simple Injector 
- AutoMapper 
- Angular 1.6 (javascript) 