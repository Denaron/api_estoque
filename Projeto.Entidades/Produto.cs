﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto.Entidades
{
    public class Produto
    {
        public virtual int IdProduto { get; set; }
        public virtual string Nome { get; set; }
        public virtual decimal Preco { get; set; }
        public virtual int Quantidade { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual int IdEstoque { get; set; }


        #region

        public virtual Estoque Estoque { get; set; }

        #endregion
    }
}
